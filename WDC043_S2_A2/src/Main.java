import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        final int[] primeNums = {2, 3, 5, 7, 11};
        final ArrayList<String> arrList = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        final HashMap<String, Integer> map = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };


        System.out.println("The first prime number is: " + primeNums[0]);
        System.out.println("The second prime number is: " + primeNums[1]);
        System.out.println("The third prime number is: " + primeNums[2]);
        System.out.println("The fourth prime number is: " + primeNums[3]);
        System.out.println("The fifth prime number is: " + primeNums[4]);

        System.out.println("\nMy friends are: " + Arrays.toString(arrList.toArray()));

        System.out.println("\nOur current inventory consists of: " + map);
    }
}