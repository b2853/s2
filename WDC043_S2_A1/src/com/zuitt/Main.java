package com.zuitt;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final Scanner scan = new Scanner(System.in);
        int year;

        System.out.print("Input year to be checked if a leap year: ");
        year = scan.nextInt();

        boolean isLeapYear = (year%4==0 && year%100!=0) || (year%4==0 && year%100==0 && year%400==0);

        if (isLeapYear) {
            System.out.println(year + " is a leap year");
        } else {
            System.out.println(year + " is NOT a leap year");
        }
    }
}